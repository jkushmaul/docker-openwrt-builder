#!/bin/bash

docker run -it --rm \
    -u $(id -u ${USER}):$(id -g ${USER}) \
    -v ${PWD}:/usr/src/openwrt/ \
    -w /usr/src/openwrt/ \
    -it openwrt_build
