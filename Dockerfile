FROM debian:9 as openwrt_build

MAINTAINER Jason Kushmaul <jasonkushmaul@gmail.com>
ENV DEBIAN_FRONTEND noninteractive

COPY ./motd /etc/motd
RUN echo "cat /etc/motd" >> /etc/bash.bashrc
RUN apt-get update  && apt-get install -y apt-utils
RUN apt-get install -y sudo git build-essential libncurses5-dev gawk git libssl-dev gettext zlib1g-dev swig unzip time python python3 wget binutils-mips-linux-gnu bison flex
RUN mkdir -p /usr/src/

RUN apt-get install -y libpam-modules  libpam-dev gnutls-bin idn2 libidn2-0 libidn2-0-dev libssh2-1-dev liblzma5 lzma-dev lzma  libsnmp-dev libboost1.62-dev libxml-parser-perl libusb-dev bin86 g++-multilib  openjdk-8-jdk



WORKDIR /usr/src/openwrt
ENTRYPOINT ["bash"]
