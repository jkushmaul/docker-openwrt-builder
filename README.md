# openwrt builder

It was difficult to get this setup and running locally so I made this.

With openwrt source as your CWD:  
`run.sh`

This will pass your CWD into the container to a shell as your current user.
You can then run:  
`./scripts/feeds install -a && ./scripts/feeds update -a && make prereq && make menuconfig && make`
